1. create a branch: git checkout -b NameTrainee (example: git checkout -b MikhailMakavetski)
2. create a submodule of the trainee repository: git submodule add URL NameSubmodule
(example: git submodule add https://gitlab.com/CRAZYMIXAKER/my-simple-pipeline MM)
3. Edit a line in composer.json
"autoload": {
  "psr-4": {
    "src\\": "NameSubmodule/"
  }
}
4. Edit a line #19 in .gitlab-ci.yml (- cd NameSubmodule/)
5. push
6. git checkout --recurse-submodules main (you can just git checkout main)

If u made a clone of a repository and there are no files in the submodule folder or you want to update files from a remote repository,
then on branch with submodule, type in terminal: git submodule update --init --recursive --remote
If u have a fatal (
fatal: Needed a single revision
Unable to find current origin/master revision in submodule path 'MikhailMakavetski'
)
U need to do that in the submodule directory git pull origin main --recurse-submodules or git fetch and git merge

If u wanna delete submodule: git rm -rf --cached NameSubmodule

To make tests through the pipeline, go to the gitlab of the repository where your submodules are located,
there is a menu on the left, go to the "CI / CD" section, then to "Pipelines". Top right, click on the blue "Run pipeline" button,
then select the branch with the required submodule for the test and click "Run pipeline"

- For trainees

1. You must create a public repository in Gitlab.
2. You should not have any subfolders, all project files with job code should be in the root of the folder.
3. For each task, create a separate file with a class, a class can have several methods with different names.
4. There must be a public method "main", which will display the result of tasks through return.
5.In each task class file, there must be "namespace src".
6. Each task file must be named "Task1, Task2...Task10".