<?php

use PHPUnit\Framework\TestCase;
use src\Task10;

class Task10Test extends TestCase
{
    /**
     * @dataProvider positiveProvider
     */
    public function testPositive(int $number, mixed $expected): void
    {
        $response = (new Task10())->main($number);
        $this::assertSame($expected, $response);
    }

    public function positiveProvider()
    {
        return [
            'Good' => [35, [
                0 => 35,
                1 => 106,
                2 => 53,
                3 => 160,
                4 => 80,
                5 => 40,
                6 => 20,
                7 => 10,
                8 => 5,
                9 => 16,
                10 => 8,
                11 => 4,
                12 => 2,
                13 => 1,
            ]],
            'Good two' => [12, [
                0 => 12,
                1 => 6,
                2 => 3,
                3 => 10,
                4 => 5,
                5 => 16,
                6 => 8,
                7 => 4,
                8 => 2,
                9 => 1, ]],
        ];
    }

    /**
     * @dataProvider negativeProvider
     */
    public function testNegative(int $number): void
    {
        $obj = new Task10();
        $this->expectException(InvalidArgumentException::class);
        $obj->main($number);
    }

    public function negativeProvider(): array
    {
        return [
            'Zero' => [0],
            'Negative number' => [-1],
        ];
    }
}
