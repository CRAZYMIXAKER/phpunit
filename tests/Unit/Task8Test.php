<?php

use PHPUnit\Framework\TestCase;
use src\Task8;

class Task8Test extends TestCase
{
    /**
     * @dataProvider positiveProvider
     */
    public function testPositive(mixed $input, mixed $expected): void
    {
        $response = (new Task8())->main($input);
        $this::assertSame($expected, $response);
    }

    public function positiveProvider(): array
    {
        return [
            'Good' => ['{
            "Title": "The Cuckoos Calling", 
            "Author": "Robert Galbraith",
            "Detail": {
            "Publisher": "Little Brown"
            }}', "Title: The Cuckoos Calling\r\nAuthor: Robert Galbraith\r\nPublisher: Little Brown"],
        ];
    }

    /**
     * @dataProvider negativeProvider
     */
    public function testNegative(mixed $input): void
    {
        $obj = new Task8();
        $this->expectException(InvalidArgumentException::class);
        $obj->main($input);
    }

    public function negativeProvider(): array
    {
        return [
            'Error' => ['Title: he Cuckoos Calling'],
            'Error Two' => [1],
            'Error three' => ['1'],
            'Error four' => [-1],
            'Error five' => [0],
        ];
    }
}
