<?php

use PHPUnit\Framework\TestCase;
use src\Task3;

class Task3Test extends TestCase
{
    /**
     * @dataProvider positiveProvider
     */
    public function testPositive(mixed $input, mixed $expected): void
    {
        $response = (new Task3())->main($input);
        $this::assertSame($expected, $response);
    }

    public function positiveProvider(): array
    {
        return [
            'Good' => [5689, 1],
            'Good Two' => [56, 2],
        ];
    }

    /**
     * @dataProvider negativeProvider
     */
    public function testNegative(mixed $input): void
    {
        $obj = new Task3();
        $this->expectException(InvalidArgumentException::class);
        $obj->main($input);
    }

    public function negativeProvider(): array
    {
        return [
            'Short' => [1],
            'Minus' => [-56],
            'Test' => [7.965],
        ];
    }
}
