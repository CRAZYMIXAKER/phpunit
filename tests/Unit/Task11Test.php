<?php

use PHPUnit\Framework\TestCase;
use src\Task11_1;

class Task11Test extends TestCase
{
    public function testSingleton(): void
    {
        $response = Task11_1::main();
        $response_1 = Task11_1::main();

        $this::assertSame($response, $response_1);
    }
}
