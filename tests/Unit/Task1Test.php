<?php

use PHPUnit\Framework\TestCase;
use src\Task1;

class Task1Test extends TestCase
{
    /**
     * @dataProvider positiveProvider
     */
    public function testGreatestNumber(int $inputNumber, string $expected): void
    {
        $response = (new Task1())->main($inputNumber);
        $this::assertSame($expected, $response);
    }

    public function positiveProvider(): array
    {
        return [
            'More than 30 Two' => [121, 'More than 30'],
            'More than 30' => [31, 'More than 30'],
            'More than 20' => [21, 'More than 20'],
            'More than 10' => [11, 'More than 10'],
            'Less than 10' => [9, 'Equal or less than 10'],
            'Check 0' => [0, 'Equal or less than 10'],
            'Check negative number' => [-10, 'Equal or less than 10'],
        ];
    }
}
