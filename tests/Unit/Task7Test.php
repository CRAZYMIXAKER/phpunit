<?php

use PHPUnit\Framework\TestCase;
use src\Task7;

class Task7Test extends TestCase
{
    /**
     * @dataProvider positiveProvider
     */
    public function testPositive(array $array, int $position, mixed $expected): void
    {
        $response = (new Task7())->main($array, $position);
        $this::assertSame($expected, $response);
    }

    public function positiveProvider(): array
    {
        return [
            'Good' => [[1, 2, 3, 4, 5], 3, [1, 2, 3, 5]],
            'Good Zero' => [[1, 2, 3, 4, 5], 0, [2, 3, 4, 5]],
        ];
    }

    /**
     * @dataProvider negativeProvider
     */
    public function testNegative(array $array, int $position): void
    {
        $obj = new Task7();
        $this->expectException(InvalidArgumentException::class);
        $obj->main($array, $position);
    }

    public function negativeProvider(): array
    {
        return [
            'Negative position' => [[1, 2], -1],
            'Short' => [[1, 2], 2],
            'Empty' => [[], 3],
            'Empty array with negative position' => [[], -1],
        ];
    }
}
