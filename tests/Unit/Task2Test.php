<?php

use PHPUnit\Framework\TestCase;
use src\Task2;

class Task2Test extends TestCase
{
    /**
     * @dataProvider positiveProvider
     */
    public function testPositive(mixed $date, mixed $expected): void
    {
        $response = (new Task2())->main($date);
        $this::assertSame($expected, $response);
    }

    public function positiveProvider(): array
    {
        $datePlus = (new DateTime('00:00'))->modify(' + 3 day')->format('d-m-Y');
        $datePast = (new DateTime('00:00'))->modify(' - 1 day')->format('d-m-Y');
        $dateNow = new DateTime('00:00');

        return [
            'Zero' => [$dateNow->format('d-m-Y'), 0],
            'Correct' => [$datePlus, 3],
            'Past' => [$datePast, 364],
        ];
    }

    /**
     * @dataProvider negativeProvider
     */
    public function testNegative(mixed $date): void
    {
        $obj = new Task2();
        $this->expectException(InvalidArgumentException::class);

        $obj->main($date);
    }

    public function negativeProvider(): array
    {
        return [
            "Data doesn't issue" => ['32-14-2022'],
            'Incorrect data' => ['0-0-0'],
        ];
    }
}
