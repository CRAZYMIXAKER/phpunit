<?php

use PHPUnit\Framework\TestCase;
use src\Task12;

class Task12Test extends TestCase
{
    /**
     * @dataProvider positiveProvider
     */
    public function testPositive(mixed $response, mixed $expected): void
    {
        $this::assertSame($expected, $response);
    }

    public function positiveProvider()
    {
        $task12 = new Task12(12, 6);

        return [
            'Add' => [array_values((array) $task12->add())[2], 18],
            'subtract' => [array_values((array) $task12->subtract())[2], 6],
            'Multiply' => [array_values((array) $task12->multiply())[2], 72],
            'Divide' => [array_values((array) $task12->divide())[2], 2],
            'DivideBy' => [array_values((array) $task12->add()->divideBy(9))[2], 2],
            'MultiplyBy' => [array_values((array) $task12->subtract()->multiplyBy(5))[2], 30],
            'SubtractBy' => [array_values((array) $task12->multiply()->subtractBy(25))[2], 47],
            'AddBy' => [array_values((array) $task12->divide()->addBy(15))[2], 17],
            'Divide Minus' => [array_values((array) (new Task12(0, -5))->divide())[2], 0],
            'DivideBy Minus' => [array_values((array) $task12->multiply()->divideBy(-12))[2], -6],
        ];
    }

    public function testNegative(): void
    {
        $task12 = new Task12(12, 6);
        $this->expectException(InvalidArgumentException::class);
        $task12->multiply()->divideBy(0);
    }

    public function testNegativeTwo(): void
    {
        $task12_1 = new Task12(-1, 0);
        $this->expectException(InvalidArgumentException::class);
        array_values((array) $task12_1->divide())[2];
    }
}
