<?php

use PHPUnit\Framework\TestCase;
use src\Task4;

class Task4Test extends TestCase
{
    /**
     * @dataProvider positiveProvider
     */
    public function testPositive(mixed $input, mixed $expected): void
    {
        $response = (new Task4())->main($input);
        $this::assertSame($expected, $response);
    }

    public function positiveProvider()
    {
        return [
            'Good' => ['The quick-brown_fox jumps over the_lazy-dog', 'TheQuickBrownFoxJumpsOverTheLazyDog'],
            'Good Two' => ['_test_was not-so Interesting_That_funny', 'TestWasNotSoInterestingThatFunny'],
        ];
    }
}
